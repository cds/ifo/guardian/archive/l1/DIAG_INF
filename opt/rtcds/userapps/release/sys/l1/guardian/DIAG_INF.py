# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, GuardStateDecorator
from guardian.manager import NodeManager
from guardian.ligopath import userapps_path
import cdsutils
import time
import math
import subprocess
from filterconfig_short import filter_list0

CHANNELS_PER_LOOP = 2001

nominal = 'RESET_INF'
request = 'RESET_INF'

class INIT(GuardState):
    index = 0
    request = True

    def run(self):
        return True

class IDLE(GuardState):
    index = 10
    goto = True

    def run(self):
        return True

class RUN_TEST(GuardState):
    index = 20
    goto = False

    def main(self):
       self.x = 0

    def run(self):
        for count in range(self.x*CHANNELS_PER_LOOP,(self.x+1)*CHANNELS_PER_LOOP):
            if count >= len(filter_list0):
                self.x = -1
                time.sleep(1)
                break
            value = ezca[filter_list0[count]]
            if value == '-inf':
                notify(filter_list0[count] + ' is -inf')
                return 'RESET_INF'
            elif float(value) == float(1e20):
                notify(filter_list0[count] + ' is 1e20')
                return 'RESET_INF'
        self.x = self.x + 1
        #Calibration filter check
        return True

#FIXME: The below state is a better way to do the above
'''
class RUN_TEST2(GuardState):
    index = 22
    goto = False

    def main(self):
       self.count = 0

    def run(self):

        if self.count < len(filter_list0):
            value = ezca[filter_list0[self.count]]
            if value == '-inf':
                notify(filter_list0[self.count] + ' is -inf')
                return 'RESET_INF'
            elif float(value) == float(1e20):
                notify(filter_list0[self.count] + ' is 1e20')
                return 'RESET_INF'
            else:
                self.count += 1
                return
        else:
            self.count = 0

        return True
'''

class RESET_INF(GuardState):
    index = 30
    goto = True

    def main(self):
        self.x = 0

    def run(self):
        for count in range(self.x*CHANNELS_PER_LOOP,(self.x+1)*CHANNELS_PER_LOOP):
            if count >= len(filter_list0):
                self.x = -1
                time.sleep(1)
                break
            value = ezca[filter_list0[count]]
            if value == '-inf':
                notify(filter_list0[count] + ' is -inf')
                ezca[filter_list0[count][:-7]+'_RSET'] = 2
            elif float(value) == float(1e20):
                notify(filter_list0[count] + ' is 1e20')
                ezca[filter_list0[count][:-7]+'_RSET'] = 2
        self.x = self.x + 1
        return True

'''
class RESET_INF2(GuardState):
    index = 33
    goto = True

    def main(self):
        self.x = 0

    def run(self):

        if self.count < len(filter_list0):
            value = ezca[filter_list0[self.count]]
            if value == '-inf':
                notify(filter_list0[self.count] + ' is -inf')
                ezca[filter_list0[self.count][:-7]+'_RSET'] = 2
            elif float(value) == float(1e20):
                notify(filter_list0[self.count] + ' is 1e20')
                ezca[filter_list0[self.count][:-7]+'_RSET'] = 2
            self.count += 1
            return

        return True
'''

edges = [
    ('INIT','IDLE'),
    ('IDLE','RUN_TEST'),
    ('RESET_INF','RUN_TEST'),
    ('RUN_TEST','IDLE'),
    ]
